/*
 * uart.c
 *
 *  Created on: Nov 9, 2018
 *      Author: User
 */

#include "driverlib.h"
#include "uart.h"
#include <string.h>
#include <msp430.h>

void configureUARTPins()
{
    GPIO_setAsPeripheralModuleFunctionInputPin(UCA0TXD_PIN);
    GPIO_setAsPeripheralModuleFunctionInputPin(UCA0RXD_PIN);
}

void configureUART()
{

    USCI_A_UART_initParam param = { 0 };
    param.selectClockSource = USCI_A_UART_CLOCKSOURCE_SMCLK;
    param.clockPrescalar = 18;                                         // UCBRx
    param.firstModReg = 0;                                             // UCBRFx
    param.secondModReg = 1;                                            // UCBRSx
    param.parity = USCI_A_UART_NO_PARITY;
    param.msborLsbFirst = USCI_A_UART_LSB_FIRST;
    param.numberofStopBits = USCI_A_UART_ONE_STOP_BIT;
    param.uartMode = USCI_A_UART_MODE;
    param.overSampling = USCI_A_UART_LOW_FREQUENCY_BAUDRATE_GENERATION; // UCOS16 = 0
    USCI_A_UART_init(USCI_A0_BASE, &param);

    //Enable UART module for operation
    USCI_A_UART_enable(USCI_A0_BASE);

    //Enable Receive Interrupt
    //USCI_A_UART_clearInterrupt(USCI_A0_BASE,
    //USCI_A_UART_RECEIVE_INTERRUPT+USCI_A_UART_TRANSMIT_INTERRUPT);
    //USCI_A_UART_enableInterrupt(USCI_A0_BASE,
    //USCI_A_UART_RECEIVE_INTERRUPT+USCI_A_UART_TRANSMIT_INTERRUPT);
    UCA0IFG &= ~UCRXIFG;
    UCA0IE |= UCRXIE;

}

void setUartTxBuffer(UARTBuffer * buffer, unsigned char * buf, int bufLen)
{
    buffer->txBuf = buf;
    buffer->txBufLen = bufLen;

    int i = 0;
    for (i = 0; i < bufLen; i++)
    {
        buf[i] = 0;
    }
}
int uartSendDataBlocking(UARTBuffer * buffer, unsigned char * buf, int len)
{
    int i = 0;
    /*
    if (len > buffer->txBufLen)
    {
        return UART_INSUFFICIENT_TX_BUF;
    }

    setUartTxBuffer(buffer, buf, len);

    buffer->txBytesToSend = len;
    buffer->txBufCtr = 0;*/
    for (i = 0; i < len; i++)
    {
        while (!(UCA0IFG & UCTXIFG))
            ;
        UCA0TXBUF = buf[i];
    }

    return UART_SUCCESS;
}
void setUartRxBuffer(UARTBuffer * buffer, unsigned char * buf, int bufLen)
{
    buffer->rxBuf = buf;
    buffer->rxBufLen = bufLen;

    int i = 0;
    for (i = 0; i < bufLen; i++)
    {
        buf[i] = 0;
    }
}

void initBufferDefaults(UARTBuffer * buffer)
{
    buffer->txBuf = NULL;
    buffer->txBufLen = 0;

    buffer->rxBuf = NULL;
    buffer->rxBufLen = 0;

    buffer->rxBytesReceived = 0;
    buffer->txBytesToSend = 0;
    buffer->txBufCtr = 0;
}

int numUartBytesReceived(UARTBuffer * buffer)
{
    return buffer->rxBytesReceived;
}

unsigned char * getUartRxBufferData(UARTBuffer * buffer)
{
    return buffer->rxBuf;
}

int uartSendDataInt(UARTBuffer * buffer, unsigned char * buf, int len)
{
    if (len > buffer->txBufLen)
    {
        return UART_INSUFFICIENT_TX_BUF;
    }

    setUartTxBuffer(buffer, buf, len);

    buffer->txBytesToSend = len;
    buffer->txBufCtr = 0;

    // Enable TX IE
    UCA0IFG &= ~UCTXIFG;
    UCA0IE |= UCTXIE;

    // Trigger the TX IFG. This will cause the Interrupt Vector to be called
    // which will send the data one byte at a time at each interrupt trigger.
    UCA0IFG |= UCTXIFG;

    return UART_SUCCESS;
}

int readRxBytes(UARTBuffer * buffer, unsigned char * data, int numBytesToRead,
                int offset)
{
    int bytes = 0;

    // Ensure we don't read past what we have in the buffer
    if (numBytesToRead + offset <= buffer->rxBytesReceived)
    {
        bytes = numBytesToRead;
    }
    else if (offset < buffer->rxBufLen)
    {
        // Since offset is valid, we provide all possible bytes until the end of the buffer
        bytes = buffer->rxBytesReceived - offset;
    }
    else
    {
        return 0;
    }

    int i = 0;
    for (i = 0; i < bytes; i++)
    {
        data[i] = buffer->rxBuf[offset + i];
    }

    // reset number of bytes available, regardless of how many bytes are left
    buffer->rxBytesReceived = 0;

    return i;

}
