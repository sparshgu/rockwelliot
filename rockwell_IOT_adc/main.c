/* --COPYRIGHT--,BSD
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/
#include <stdlib.h>
#include "sensorAdc.h"
#include "sensorActivatingPulse.h"
#include "uart.h"
#include <string.h>
//******************************************************************************
//!
//!   Empty Project that includes driverlib
//!
//******************************************************************************

UARTBuffer buffer;
unsigned char uartTxBuf[250];
unsigned char uartRxBuf[250];
unsigned char tempBuf[100];
void main(void)
{
    char resultStr[5];

    //Stop WDT
    WDT_A_hold(WDT_A_BASE);

    setPulseOutputPin();

    configureUARTPins();

    configureUART();

    initBufferDefaults(&buffer);

    setUartTxBuffer(&buffer, uartTxBuf, 250);
    setUartRxBuffer(&buffer, uartRxBuf, 250);

    configureTimerForSensorPulse();

    configureADC();

    outputPulse();

    while (1)
    {
        if (adcSensor1Enable == 0)
        {
            startADC();
        }
        if (adcSensor1Enable == 0) // && adcSensor2Enable == 0
        //&& adcSensor3Enable == 0)
        {
            ltoa(results[0], resultStr);
            buffer.txBufLen = 250;
            uartSendDataBlocking(&buffer, (unsigned char *) resultStr,
                                 strlen(resultStr));
            //adcSensor1Enable = 1;
        }
        //Enter LPM4, Enable interrupts
        __bis_SR_register(LPM4_bits + GIE);
        //For debugger
        __no_operation();
    }

}
