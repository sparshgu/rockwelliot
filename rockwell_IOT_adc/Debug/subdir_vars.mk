################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../lnk_msp430f5529.cmd 

C_SRCS += \
../loraSerialCommunication.c \
../main.c \
../sensorActivatingPulse.c \
../sensorAdc.c \
../uart.c 

C_DEPS += \
./loraSerialCommunication.d \
./main.d \
./sensorActivatingPulse.d \
./sensorAdc.d \
./uart.d 

OBJS += \
./loraSerialCommunication.obj \
./main.obj \
./sensorActivatingPulse.obj \
./sensorAdc.obj \
./uart.obj 

OBJS__QUOTED += \
"loraSerialCommunication.obj" \
"main.obj" \
"sensorActivatingPulse.obj" \
"sensorAdc.obj" \
"uart.obj" 

C_DEPS__QUOTED += \
"loraSerialCommunication.d" \
"main.d" \
"sensorActivatingPulse.d" \
"sensorAdc.d" \
"uart.d" 

C_SRCS__QUOTED += \
"../loraSerialCommunication.c" \
"../main.c" \
"../sensorActivatingPulse.c" \
"../sensorAdc.c" \
"../uart.c" 


