/*
 * sensorAdc.c
 *
 *  Created on: Nov 14, 2018
 *      Author: User
 */

#include "driverlib.h"
#include "sensorAdc.h"

volatile uint16_t results[3];

void setAdcChannels()
{
    //Enable A/D channel A0
    GPIO_setAsPeripheralModuleFunctionInputPin(
            GPIO_PORT_P6, GPIO_PIN0 + GPIO_PIN1 + GPIO_PIN2);
}

void configureADC()
{
    setAdcChannels();

    //Initialize the ADC12_A Module
    /*
     * Base address of ADC12_A Module
     * Use internal ADC12_A bit as sample/hold signal to start conversion
     * USE MODOSC 5MHZ Digital Oscillator as clock source
     * Use default clock divider of 1
     */
    ADC12_A_init(ADC12_A_BASE,
    ADC12_A_SAMPLEHOLDSOURCE_SC,
                 ADC12_A_CLOCKSOURCE_ADC12OSC,
                 ADC12_A_CLOCKDIVIDER_1);

    ADC12_A_enable(ADC12_A_BASE);

    /*
     * Base address of ADC12_A Module
     * For memory buffers 0-7 sample/hold for 1024 clock cycles
     * For memory buffers 8-15 sample/hold for 4 clock cycles (default)
     * Enable Multiple Sampling
     */
    ADC12_A_setupSamplingTimer(ADC12_A_BASE,
    ADC12_A_CYCLEHOLD_1024_CYCLES,
                               ADC12_A_CYCLEHOLD_4_CYCLES,
                               ADC12_A_MULTIPLESAMPLESENABLE);

    ADC12_A_configureMemoryParam param0 = { 0 };
    param0.memoryBufferControlIndex = ADC12_A_MEMORY_0;
    param0.inputSourceSelect = ADC12_A_INPUT_A0;
    param0.positiveRefVoltageSourceSelect = ADC12_A_VREFPOS_AVCC;
    param0.negativeRefVoltageSourceSelect = ADC12_A_VREFNEG_AVSS;
    param0.endOfSequence = ADC12_A_NOTENDOFSEQUENCE;
    ADC12_A_configureMemory(ADC12_A_BASE, &param0);

    ADC12_A_configureMemoryParam param1 = { 0 };
    param1.memoryBufferControlIndex = ADC12_A_MEMORY_1;
    param1.inputSourceSelect = ADC12_A_INPUT_A1;
    param1.positiveRefVoltageSourceSelect = ADC12_A_VREFPOS_AVCC;
    param1.negativeRefVoltageSourceSelect = ADC12_A_VREFNEG_AVSS;
    param1.endOfSequence = ADC12_A_NOTENDOFSEQUENCE;
    ADC12_A_configureMemory(ADC12_A_BASE, &param1);

    ADC12_A_configureMemoryParam param2 = { 0 };
    param2.memoryBufferControlIndex = ADC12_A_MEMORY_2;
    param2.inputSourceSelect = ADC12_A_INPUT_A2;
    param2.positiveRefVoltageSourceSelect = ADC12_A_VREFPOS_AVCC;
    param2.negativeRefVoltageSourceSelect = ADC12_A_VREFNEG_AVSS;
    param2.endOfSequence = ADC12_A_ENDOFSEQUENCE;
    ADC12_A_configureMemory(ADC12_A_BASE, &param2);

    //Enable memory buffer 0 interrupt
    ADC12_A_clearInterrupt(ADC12_A_BASE,
    ADC12IFG0);
    ADC12_A_enableInterrupt(ADC12_A_BASE,
    ADC12IE0);
}

inline void startADC()
{
    ADC12_A_startConversion(ADC12_A_BASE,
    ADC12_A_MEMORY_0,
                            ADC12_A_SEQOFCHANNELS);
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=ADC12_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(ADC12_VECTOR)))
#endif
void ADC12ISR(void)
{
    switch (__even_in_range(ADC12IV, 34))
    {
    case 0:
        break;   //Vector  0:  No interrupt
    case 2:
        break;   //Vector  2:  ADC overflow
    case 4:
        break;   //Vector  4:  ADC timing overflow
    case 6:
        //Move results, IFG is cleared
        results[0] = ADC12_A_getResults(ADC12_A_BASE,
        ADC12_A_MEMORY_0);
        //Move results, IFG is cleared
        results[1] = ADC12_A_getResults(ADC12_A_BASE,
        ADC12_A_MEMORY_1);
        //Move results, IFG is cleared
        results[2] = ADC12_A_getResults(ADC12_A_BASE,
        ADC12_A_MEMORY_2);

        //Exit active CPU,
        //SET BREAKPOINT HERE and watch results[]
        __bic_SR_register_on_exit(LPM4_bits);
        break;   //Vector  6:  ADC12IFG0
    case 8:          //Vector  8:  ADC12IFG1
        break;
    case 10:
        break;   //Vector 10:  ADC12IFG2
    case 12:
        break;   //Vector 12:  ADC12IFG3
    case 14:
        break;   //Vector 14:  ADC12IFG4
    case 16:
        break;   //Vector 16:  ADC12IFG5
    case 18:
        break;   //Vector 18:  ADC12IFG6
    case 20:
        break;   //Vector 20:  ADC12IFG7
    case 22:
        break;   //Vector 22:  ADC12IFG8
    case 24:
        break;   //Vector 24:  ADC12IFG9
    case 26:
        break;   //Vector 26:  ADC12IFG10
    case 28:
        break;   //Vector 28:  ADC12IFG11
    case 30:
        break;   //Vector 30:  ADC12IFG12
    case 32:
        break;   //Vector 32:  ADC12IFG13
    case 34:
        break;   //Vector 34:  ADC12IFG14
    default:
        break;
    }
}
