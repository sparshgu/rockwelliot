/*
 * uart.h
 *
 *  Created on: Nov 9, 2018
 *      Author: User
 */

#ifndef UART_H_
#define UART_H_

#define UCA0TXD_PIN     GPIO_PORT_P3,GPIO_PIN3
#define UCA0RXD_PIN     GPIO_PORT_P3,GPIO_PIN4
enum UART_ERR_CODES
{
    UART_SUCCESS = 0,
    UART_BAD_MODULE_NAME,
    UART_BAD_CLK_SOURCE,
    UART_INSUFFICIENT_TX_BUF,
    UART_INSUFFICIENT_RX_BUF,
    UART_BAD_PORT_SELECTED
};
typedef struct
{
    unsigned char * txBuf;
    unsigned char * rxBuf;
    int txBufLen;
    int rxBufLen;
    int rxBytesReceived;
    int txBytesToSend;
    int txBufCtr;
} UARTBuffer;

void initBufferDefaults(UARTBuffer * buffer);
void configureUART();
void configureUARTPins();
int uartSendDataBlocking(UARTBuffer * buffer, unsigned char * buf, int len);
int uartSendDataInt(UARTBuffer * buffer, unsigned char * buf, int len);
unsigned char * getUartRxBufferData(UARTBuffer * buffer);
int numUartBytesReceived(UARTBuffer * buffer);
void initBufferDefaults(UARTBuffer * buffer);
void setUartRxBuffer(UARTBuffer * buffer, unsigned char * buf, int bufLen);
void setUartTxBuffer(UARTBuffer * buffer, unsigned char * buf, int bufLen);
int readRxBytes(UARTBuffer * buffer, unsigned char * data, int numBytesToRead,
                int offset);
void enableUARTReceiveInterrupt();
#endif /* UART_H_ */
