/*
 * sensorActivatingPulse.c
 *
 *  Created on: Nov 9, 2018
 *      Author: User
 */

#include "sensorActivatingPulse.h"
#include "driverlib.h"

volatile int adcSensor1Enable = 1;
volatile int adcSensor2Enable = 1;
volatile int adcSensor3Enable = 1;

void setPulseOutputPin()
{
    //P1.2,P1.3,P1.4 as PWM output
    //GPIO_setAsPeripheralModuleFunctionOutputPin(
    //GPIO_PORT_P1,
    //GPIO_PIN0);
    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN2);
    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0);

}

void configureTimerForPWM(OutputPWMParams* param)
{
    Timer_A_outputPWMParam param0;
    param0.clockSource = TIMER_A_CLOCKSOURCE_ACLK;
    param0.clockSourceDivider = TIMER_A_CLOCKSOURCE_DIVIDER_1;
    param0.timerPeriod = TIMER_PERIOD;
    param0.compareRegister = TIMER_A_CAPTURECOMPARE_REGISTER_1;
    param0.compareOutputMode = TIMER_A_OUTPUTMODE_SET_RESET;
    param0.dutyCycle = DUTY_CYCLE;
    param->param0 = &param0;

    /*
     Timer_A_outputPWMParam param1;
     param1.clockSource = TIMER_A_CLOCKSOURCE_ACLK;
     param1.clockSourceDivider = TIMER_A_CLOCKSOURCE_DIVIDER_1;
     param1.timerPeriod = TIMER_PERIOD;
     param1.compareRegister = TIMER_A_CAPTURECOMPARE_REGISTER_2;
     param1.compareOutputMode = TIMER_A_OUTPUTMODE_RESET_SET;
     param1.dutyCycle = DUTY_CYCLE;
     param->param1 = &param1;

     Timer_A_outputPWMParam param2;
     param2.clockSource = TIMER_A_CLOCKSOURCE_ACLK;
     param2.clockSourceDivider = TIMER_A_CLOCKSOURCE_DIVIDER_1;
     param2.timerPeriod = TIMER_PERIOD;
     param2.compareRegister = TIMER_A_CAPTURECOMPARE_REGISTER_3;
     param2.compareOutputMode = TIMER_A_OUTPUTMODE_RESET_SET;
     param2.dutyCycle = DUTY_CYCLE;
     param->param2 = &param2;
     */

}

void outputPWM(Timer_A_outputPWMParam* param)
{
    Timer_A_outputPWM(TIMER_A1_BASE, param);
}

void configureTimerForSensorPulse()
{
//    Timer_A_initUpDownModeParam initUpDownParam = { 0 };
//    initUpDownParam.clockSource = TIMER_A_CLOCKSOURCE_ACLK;
//    initUpDownParam.clockSourceDivider = TIMER_A_CLOCKSOURCE_DIVIDER_10;
//    initUpDownParam.timerPeriod = TIMER_PERIOD;
//    initUpDownParam.timerInterruptEnable_TAIE = TIMER_A_TAIE_INTERRUPT_DISABLE;
//    initUpDownParam.captureCompareInterruptEnable_CCR0_CCIE =
//    TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE;
//    initUpDownParam.timerClear = TIMER_A_DO_CLEAR;
//    initUpDownParam.startTimer = false;
//    Timer_A_initUpDownMode(TIMER_A0_BASE, &initUpDownParam);

    Timer_A_initUpModeParam initUpDownParam = { 0 };
    initUpDownParam.clockSource = TIMER_A_CLOCKSOURCE_ACLK;
    initUpDownParam.clockSourceDivider = TIMER_A_CLOCKSOURCE_DIVIDER_10;
    initUpDownParam.timerPeriod = TIMER_PERIOD;
    initUpDownParam.timerInterruptEnable_TAIE = TIMER_A_TAIE_INTERRUPT_ENABLE;
    initUpDownParam.captureCompareInterruptEnable_CCR0_CCIE =
    TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE;
    initUpDownParam.timerClear = TIMER_A_DO_CLEAR;
    initUpDownParam.startTimer = false;
    Timer_A_initUpMode(TIMER_A0_BASE, &initUpDownParam);

    //Initialze compare registers to generate PWM1

    Timer_A_initCompareModeParam initComp1Param = { 0 };
    initComp1Param.compareRegister = TIMER_A_CAPTURECOMPARE_REGISTER_1;
    initComp1Param.compareInterruptEnable =
    TIMER_A_CAPTURECOMPARE_INTERRUPT_ENABLE;
    initComp1Param.compareOutputMode = TIMER_A_OUTPUTMODE_RESET_SET;
    initComp1Param.compareValue = DUTY_CYCLE;
    Timer_A_initCompareMode(TIMER_A0_BASE, &initComp1Param);
    /*
     //Initialze compare registers to generate PWM2
     Timer_A_initCompareModeParam initComp2Param = { 0 };
     initComp2Param.compareRegister = TIMER_A_CAPTURECOMPARE_REGISTER_2;
     initComp2Param.compareInterruptEnable =
     TIMER_A_CAPTURECOMPARE_INTERRUPT_ENABLE;
     initComp2Param.compareOutputMode = TIMER_A_OUTPUTMODE_RESET_SET;
     initComp2Param.compareValue = DUTY_CYCLE;
     Timer_A_initCompareMode(TIMER_A0_BASE, &initComp2Param);

     //Initialze compare registers to generate PWM3
     Timer_A_initCompareModeParam initComp3Param = { 0 };
     initComp3Param.compareRegister = TIMER_A_CAPTURECOMPARE_REGISTER_3;
     initComp3Param.compareInterruptEnable =
     TIMER_A_CAPTURECOMPARE_INTERRUPT_ENABLE;
     initComp3Param.compareOutputMode = TIMER_A_OUTPUTMODE_RESET_SET;
     initComp3Param.compareValue = DUTY_CYCLE;
     Timer_A_initCompareMode(TIMER_A0_BASE, &initComp3Param);
     */
}

void outputPulse()
{
    Timer_A_startCounter(TIMER_A0_BASE,
    TIMER_A_UP_MODE);
    adcSensor1Enable ^= 1;
}
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=TIMER0_A0_VECTOR
__interrupt



#elif defined(__GNUC__)
__attribute__((interrupt(TIMER0_A0_VECTOR)))


#endif
__interrupt
void myISR_TA0_CCR0(void)
{
    GPIO_toggleOutputOnPin(GPIO_PORT_P1,
    GPIO_PIN2);
    GPIO_toggleOutputOnPin(GPIO_PORT_P1,
        GPIO_PIN0);
    adcSensor1Enable ^= 1;
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=TIMER0_A1_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(TIMER0_A1_VECTOR)))
#endif
void TIMER1_A1_ISR(void)
{
    switch (__even_in_range(TA0IV, 10))
    {
    case 0x00:
        break;
    case 0x02:
        GPIO_toggleOutputOnPin(GPIO_PORT_P1,
        GPIO_PIN2);
        GPIO_toggleOutputOnPin(GPIO_PORT_P1,
                GPIO_PIN0);
        adcSensor1Enable ^= 1;
        break;
    case 0x04:
        //adcSensor2Enable = 0;
        break;
    case 0x06:
        //adcSensor3Enable = 0;
        break;             // CCR3 IFG
    case 0x08:
        break;             // CCR4 IFG
    case 0x0A:
        break;             // CCR5 IFG
    case 0x0C:
        break;             // CCR6 IFG
    case 0x0E:                    // TA0IFG
        break;
    default:
        break;

    }
    __bic_SR_register_on_exit(LPM4_bits);
}
