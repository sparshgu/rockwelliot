/*
 * loraSerialCommunication.h
 *
 *  Created on: Nov 9, 2018
 *      Author: User
 */

#ifndef LORASERIALCOMMUNICATION_H_
#define LORASERIALCOMMUNICATION_H_

enum LORA_ERR_CODES
{
    LORA_RESPONSE_SUCCESS = 0,
    LORA_NO_RESPONSE = 1,
    LORA_NO_RESPONSE_BAD_PACKET = 2,
    LORA_INVALID_PARAM = 3,
    LORA_MAC_CONFIG_ERR = 4
};

static unsigned char invalidLoraResponse[] = "invalid_param";
static const char SetNetworkKey[] =
        "mac set nwkskey 2b7e151628aed2a6abf7158809cf4f3c\r\n";
static const char SetAppsKey[] =
        "mac set appskey 3C8F262739BFE3B7BC0826991AD0504D\r\n";
static const char SetDeviceAddress[] = "mac set devaddr 001C0EE9\r\n";
static const char SetSaveMac[] = "mac save\r\n";
static const char SetJoinABP[] = "mac join abp\r\n";
static const char SetTxUncf[] = "mac tx uncnf %s %s\r\n$";
static const char SetMacReset[] = "mac reset\r\n";
static const char GetVer[] = "sys get ver\r\n";
static const char GetVdd[] = "sys get vdd\r\n";

void setUpLoraUARTCommunication();
int resetLoraRNModule();
int setUpLoraToCommunicateWithGateway();
int saveLoraGatewayConfigSave();
int readResponseFromLORA(int bytesAvailable);
int joinMacABP();
int getSystemVdd();
int getSystemVer();
int trasnmitUncfData(char* sequenceNumber, char* value);

#endif /* LORASERIALCOMMUNICATION_H_ */
