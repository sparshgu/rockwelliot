/*
 * loraSerialCommunication.c
 *
 *  Created on: Nov 9, 2018
 *      Author: User
 */
#include "uart.h"
#include <msp430.h>
#include "driverlib.h"
#include <string.h>
#include <stdio.h>
#include "loraSerialCommunication.h"

UARTBuffer buffer;
unsigned char uartTxBuf[250];
unsigned char uartRxBuf[250];
unsigned char tempBuf[100];
char* token;

void setUpLoraUARTCommunication()
{

    configureUARTPins();

    configureUART();

    initBufferDefaults(&buffer);

    setUartTxBuffer(&buffer, uartTxBuf, 250);
    setUartRxBuffer(&buffer, uartRxBuf, 250);

}

int getSystemVdd()
{
    uartSendDataInt(&buffer, (unsigned char *) GetVdd, strlen(GetVdd));
    __delay_cycles(100000);
    int bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        return readResponseFromLORA(bytesAvailable);

    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }

}

int getSystemVer()
{
    uartSendDataInt(&buffer, (unsigned char *) GetVer, strlen(GetVer));
    __delay_cycles(100000);
    int bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        return readResponseFromLORA(bytesAvailable);
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }

}
int resetLoraRNModule()
{
    buffer.txBufLen = 250;
    uartSendDataInt(&buffer, (unsigned char *) SetMacReset,
                    strlen(SetMacReset));
    __delay_cycles(100000);
    int bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        return readResponseFromLORA(bytesAvailable);
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }
}
int joinMacABP()
{
    buffer.txBufLen = 250;
    uartSendDataInt(&buffer, (unsigned char *) SetJoinABP, strlen(SetJoinABP));
    __delay_cycles(2000000);
    int status = LORA_NO_RESPONSE_BAD_PACKET;
    volatile int bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        status = readResponseFromLORA(bytesAvailable);
        return status;
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }

}
int setUpLoraToCommunicateWithGateway()
{
    uartSendDataInt(&buffer, (unsigned char *) SetNetworkKey,
                    strlen(SetNetworkKey));
    __delay_cycles(100000);
    volatile int bytesAvailable = numUartBytesReceived(&buffer);
    int status = LORA_NO_RESPONSE_BAD_PACKET;
    if (bytesAvailable > 0)
    {
        status = readResponseFromLORA(bytesAvailable);
        if (status == LORA_RESPONSE_SUCCESS)
        {
            status = LORA_NO_RESPONSE_BAD_PACKET;
            buffer.txBufLen = 250;
            uartSendDataInt(&buffer, (unsigned char *) SetAppsKey,
                            strlen(SetAppsKey));
            __delay_cycles(100000);
            bytesAvailable = numUartBytesReceived(&buffer);
            if (bytesAvailable > 0)
            {
                status = readResponseFromLORA(bytesAvailable);
                if (status == LORA_RESPONSE_SUCCESS)
                {
                    status = LORA_NO_RESPONSE_BAD_PACKET;
                    buffer.txBufLen = 250;
                    uartSendDataInt(&buffer, (unsigned char *) SetDeviceAddress,
                                    strlen(SetDeviceAddress));
                    __delay_cycles(100000);
                    bytesAvailable = numUartBytesReceived(&buffer);
                    if (bytesAvailable > 0)
                    {
                        status = readResponseFromLORA(bytesAvailable);
                        if (status == LORA_RESPONSE_SUCCESS)
                        {

                            status = LORA_NO_RESPONSE_BAD_PACKET;
                            buffer.txBufLen = 250;
                            uartSendDataInt(&buffer,
                                            (unsigned char *) SetSaveMac,
                                            strlen(SetSaveMac));
                            __delay_cycles(1000000);
                            bytesAvailable = numUartBytesReceived(&buffer);
                            if (bytesAvailable > 0)
                            {
                                status = readResponseFromLORA(bytesAvailable);
                                if (status == LORA_RESPONSE_SUCCESS)
                                {
                                    status = LORA_NO_RESPONSE_BAD_PACKET;
                                    buffer.txBufLen = 250;
                                    uartSendDataInt(
                                            &buffer,
                                            (unsigned char *) SetJoinABP,
                                            strlen(SetJoinABP));
                                    __delay_cycles(1000000);
                                    bytesAvailable = numUartBytesReceived(
                                            &buffer);
                                    bytesAvailable = numUartBytesReceived(
                                            &buffer);
                                    if (bytesAvailable > 0)
                                    {
                                        status = readResponseFromLORA(
                                                bytesAvailable);
                                        return status;
                                    }
                                    else
                                    {
                                        return LORA_NO_RESPONSE_BAD_PACKET;
                                    }

                                }
                                else
                                {
                                    return LORA_NO_RESPONSE_BAD_PACKET;
                                }

                            }
                            else
                            {
                                return LORA_NO_RESPONSE_BAD_PACKET;
                            }

                        }
                        else
                        {
                            return LORA_NO_RESPONSE_BAD_PACKET;
                        }
                    }
                    else
                    {
                        return LORA_NO_RESPONSE_BAD_PACKET;
                    }

                }
                else
                {
                    return status;
                }
            }
            else
            {
                return LORA_NO_RESPONSE_BAD_PACKET;
            }
        }
        else
        {
            return status;
        }
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }

}
int trasnmitUncfData(char* sequenceNumber, char* value)
{
    char transmitString[100];
    memset(transmitString, 0, 100);
    snprintf(transmitString, 100, SetTxUncf, sequenceNumber, value);
    token = strtok(transmitString, "$");
    buffer.txBufLen = 250;
    uartSendDataBlocking(&buffer, (unsigned char *) token, strlen(token));
    __delay_cycles(100000);
    int status = LORA_NO_RESPONSE_BAD_PACKET;
    int bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        status = readResponseFromLORA(bytesAvailable);
        if (status == LORA_RESPONSE_SUCCESS)
        {
            return status;
        }
        else
        {
            return LORA_NO_RESPONSE_BAD_PACKET;
        }
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }

}

int saveLoraGatewayConfigSave()
{
    uartSendDataInt(&buffer, (unsigned char *) SetSaveMac, strlen(SetSaveMac));
    __delay_cycles(100000);
    int bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        return readResponseFromLORA(bytesAvailable);
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }

}
int readResponseFromLORA(int bytesAvailable)
{
    memset(tempBuf, 0, 100);
    volatile int bytesRead = readRxBytes(&buffer, tempBuf, bytesAvailable, 0);
    if (bytesRead == bytesAvailable)
    {
        if ((tempBuf[0] == invalidLoraResponse[0])
                && (tempBuf[7] == invalidLoraResponse[7])
                && (tempBuf[12] == invalidLoraResponse[12]))
        {
            return LORA_INVALID_PARAM;
        }
        if ((tempBuf[0] == 'e') && (tempBuf[1] == 'r') && (tempBuf[2] == 'r'))
        {
            return LORA_MAC_CONFIG_ERR;
        }
        return LORA_RESPONSE_SUCCESS;
    }
    else
    {
        return LORA_NO_RESPONSE;
    }
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=USCI_A0_VECTOR
__interrupt void USCI_A0_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(USCIAB0RX_VECTOR))) USCI0RX_ISR (void)
#else
#error Compiler not supported!
#endif
{
    switch (__even_in_range(UCA0IV, 4))
    {
    case 0:
        break;                             // Vector 0 - no interrupt
    case 2:                                // Vector 2 - RXIFG
        buffer.rxBuf[buffer.rxBytesReceived] = UCA0RXBUF;
        buffer.rxBytesReceived++;

        // If the received bytes filled up the buffer, go back to beginning
        if (buffer.rxBytesReceived > buffer.rxBufLen)
        {
            buffer.rxBytesReceived = 0;
        }
        break;
    case 4:                               // Vector 4 - TXIFG
        if (buffer.txBytesToSend > 0)
        {
            UCA0TXBUF = buffer.txBuf[buffer.txBufCtr];
            buffer.txBufCtr++;

            // If we've sent all the bytes, set counter to 0 to stop the sending
            if (buffer.txBufCtr == buffer.txBytesToSend)
            {
                buffer.txBufCtr = 0;

                // Disable TX IE
                UCA0IE &= ~UCTXIE;

                // Clear TX IFG
                UCA0IFG &= ~UCTXIFG;

            }
        }
        break;
    default:
        break;
    }
}
