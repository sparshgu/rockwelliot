/*
 * sensorAdc.h
 *
 *  Created on: Nov 14, 2018
 *      Author: User
 */

#ifndef SENSORADC_H_
#define SENSORADC_H_

extern volatile unsigned int results[3];
void setAdcChannels();
void configureADC();
void startADC();
#endif /* SENSORADC_H_ */
