/*
 * loraSerialCommunication.c
 *
 *  Created on: Nov 9, 2018
 *      Author: User
 */
#include "uart.h"
#include <msp430.h>
#include "driverlib.h"
#include <string.h>
#include <stdio.h>
#include "board.h"
#include "loraSerialCommunication.h"

UARTBuffer buffer;
unsigned char uartTxBuf[250];
unsigned char uartRxBuf[250];
unsigned char tempBuf[100];
char* token;

void setUpLoraUARTCommunication()
{

    configureUARTPins();

    configureUART();

    initBufferDefaults(&buffer);

    setUartTxBuffer(&buffer, uartTxBuf, 250);
    setUartRxBuffer(&buffer, uartRxBuf, 250);

}

int getSystemVdd()
{
    uartSendDataInt(&buffer, (unsigned char *) GetVdd, strlen(GetVdd));
    __delay_cycles(100000);
    int bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        return readResponseFromLORA(bytesAvailable);
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }

}

int getSystemVer()
{
    uartSendDataInt(&buffer, (unsigned char *) GetVer, strlen(GetVer));
    __delay_cycles(100000);
    int bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        return readResponseFromLORA(bytesAvailable);
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }

}
int resetLoraRNModule()
{
    buffer.txBufLen = 250;
    uartSendDataInt(&buffer, (unsigned char *) SetMacReset,
                    strlen(SetMacReset));
    __delay_cycles(100000);
    int bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        return readResponseFromLORA(bytesAvailable);
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }
}
int joinMacABP()
{
    buffer.txBufLen = 250;
    uartSendDataInt(&buffer, (unsigned char *) SetJoinABP, strlen(SetJoinABP));
    __delay_cycles(2000000);
    int status = LORA_NO_RESPONSE_BAD_PACKET;
    volatile int bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        status = readResponseFromLORA(bytesAvailable);
        return status;
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }

}

int joinMacOTAA()
{
    buffer.txBufLen = 250;
    uartSendDataInt(&buffer, (unsigned char *) SetmacOTAAJoin,
                    strlen(SetmacOTAAJoin));
    __delay_cycles(7000000);
    int status = LORA_NO_RESPONSE_BAD_PACKET;
    volatile int bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        status = readResponseFromLORA(bytesAvailable);
        if (status == LORA_RESPONSE_SUCCESS)
        {
            if (buffer.rxBuf[0] == 'o' && buffer.rxBuf[1] == 'k'
                    && buffer.rxBuf[4] == 'a' && buffer.rxBuf[9] == 'd')
            {
                return LORA_RESPONSE_SUCCESS;
            }
            else
            {
                return LORA_INVALID_PARAM;
            }
        }
        else
        {

            return status;
        }
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }

}

#if  !LoraWAN
int setRadioPower(char* power)
{
    char transmitString[100];
    int bytesAvailable = -1;
    memset(transmitString, 0, 100);
    snprintf(transmitString, 100, SetRadioPwr, power);
    token = strtok(transmitString, "$");
    buffer.txBufLen = 250;
    uartSendDataBlocking(&buffer, (unsigned char *) token, strlen(token));
    __delay_cycles(100000);
    bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        return readResponseFromLORA(bytesAvailable);
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }

}

int setRadioBandWidth(char* bW)
{
    char transmitString[100];
    int bytesAvailable = -1;
    memset(transmitString, 0, 100);
    snprintf(transmitString, 100, SetRadioBW, bW);
    token = strtok(transmitString, "$");
    buffer.txBufLen = 250;
    uartSendDataBlocking(&buffer, (unsigned char *) token, strlen(token));
    __delay_cycles(100000);
    bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        return readResponseFromLORA(bytesAvailable);
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }

}
int setRadioCenterFreq(char* freq)
{
    char transmitString[100];
    int bytesAvailable = -1;
    memset(transmitString, 0, 100);
    snprintf(transmitString, 100, SetRadioCenterFreq, freq);
    token = strtok(transmitString, "$");
    buffer.txBufLen = 250;
    uartSendDataBlocking(&buffer, (unsigned char *) token, strlen(token));
    __delay_cycles(100000);
    bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        return readResponseFromLORA(bytesAvailable);
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }

}
int setRadioSpread(char* spread)
{
    char transmitString[100];
    int bytesAvailable = -1;
    memset(transmitString, 0, 100);
    snprintf(transmitString, 100, SetRadioSpread, spread);
    token = strtok(transmitString, "$");
    buffer.txBufLen = 250;
    uartSendDataBlocking(&buffer, (unsigned char *) token, strlen(token));
    __delay_cycles(100000);
    bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        return readResponseFromLORA(bytesAvailable);
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }

}
int setUpLoraToCommunicateWithGateway()
{

    uartSendDataInt(&buffer, (unsigned char *) SetRadioMode,
                    strlen(SetRadioMode));
    __delay_cycles(100000);
    volatile int bytesAvailable = numUartBytesReceived(&buffer);
    int status = LORA_NO_RESPONSE_BAD_PACKET;
    if (bytesAvailable > 0)
    {
        status = readResponseFromLORA(bytesAvailable);
        if (status == LORA_RESPONSE_SUCCESS)
        {
            status = LORA_NO_RESPONSE_BAD_PACKET;
            status = setRadioPower(RadioPower_20);
            if (status == LORA_RESPONSE_SUCCESS)
            {

                status = LORA_NO_RESPONSE_BAD_PACKET;
                status = setRadioCenterFreq("902300000");
                if (status == LORA_RESPONSE_SUCCESS)
                {
                    status = LORA_NO_RESPONSE_BAD_PACKET;
                    buffer.txBufLen = 250;
                    status = LORA_NO_RESPONSE_BAD_PACKET;
                    buffer.txBufLen = 250;
                    uartSendDataInt(&buffer, (unsigned char *) SetRadioCR,
                                    strlen(SetRadioCR));
                    __delay_cycles(100000);
                    if (bytesAvailable > 0)
                    {

                        status = readResponseFromLORA(bytesAvailable);
                        if (status == LORA_RESPONSE_SUCCESS)
                        {

                            status = LORA_NO_RESPONSE_BAD_PACKET;
                            status = setRadioSpread(SF12);
                            if (status == LORA_RESPONSE_SUCCESS)
                            {
                                status = LORA_NO_RESPONSE_BAD_PACKET;
                                status = setRadioBandWidth(BW_125);
                                if (status == LORA_RESPONSE_SUCCESS)
                                {
                                    status = LORA_NO_RESPONSE_BAD_PACKET;
                                    buffer.txBufLen = 250;
                                    status = LORA_NO_RESPONSE_BAD_PACKET;
                                    buffer.txBufLen = 250;
                                    uartSendDataInt(
                                            &buffer,
                                            (unsigned char *) SetRadioCRC,
                                            strlen(SetRadioCRC));
                                    __delay_cycles(100000);
                                    bytesAvailable = numUartBytesReceived(
                                            &buffer);
                                    if (bytesAvailable > 0)
                                    {
                                        status = readResponseFromLORA(
                                                bytesAvailable);
                                        return status;
                                    }
                                    else
                                    {
                                        return LORA_NO_RESPONSE_BAD_PACKET;
                                    }

                                }
                                else
                                {
                                    return status;
                                }

                            }
                            else
                            {
                                return status;
                            }

                        }
                        else
                        {
                            return status;
                        }

                    }
                    else
                    {
                        return LORA_NO_RESPONSE_BAD_PACKET;
                    }

                }
                else
                {
                    return status;
                }

            }
            else
            {
                return status;
            }

        }
        else
        {
            return status;
        }

    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }

}
#endif
int setUpLoraWANToCommunicateWithGateway(int mode)
{

    if (mode == ABP)
    {

        uartSendDataInt(&buffer, (unsigned char *) SetNetworkKey,
                        strlen(SetNetworkKey));
    }
    else if (mode == OTAA)
    {
        uartSendDataInt(&buffer, (unsigned char *) SetDevEui,
                        strlen(SetDevEui));
    }
    else
    {
        return LORA_CONFIG_ERR;
    }
    __delay_cycles(100000);
    volatile int bytesAvailable = numUartBytesReceived(&buffer);
    int status = LORA_NO_RESPONSE_BAD_PACKET;
    if (bytesAvailable > 0)
    {
        status = readResponseFromLORA(bytesAvailable);
        if (status == LORA_RESPONSE_SUCCESS)
        {
            status = LORA_NO_RESPONSE_BAD_PACKET;
            buffer.txBufLen = 250;
            if (mode == ABP)
            {

                uartSendDataInt(&buffer, (unsigned char *) SetAppsKey,
                                strlen(SetAppsKey));
            }
            else if (mode == OTAA)
            {
                uartSendDataInt(&buffer, (unsigned char *) SetAppEui,
                                strlen(SetAppEui));
            }
            else
            {
                return LORA_CONFIG_ERR;
            }

            __delay_cycles(100000);
            bytesAvailable = numUartBytesReceived(&buffer);
            if (bytesAvailable > 0)
            {
                status = readResponseFromLORA(bytesAvailable);
                if (status == LORA_RESPONSE_SUCCESS)
                {
                    status = LORA_NO_RESPONSE_BAD_PACKET;
                    buffer.txBufLen = 250;
                    if (mode == ABP)
                    {

                        uartSendDataInt(&buffer,
                                        (unsigned char *) SetDeviceAddress,
                                        strlen(SetDeviceAddress));
                    }
                    else if (mode == OTAA)
                    {
                        uartSendDataInt(&buffer, (unsigned char *) SetAppKey,
                                        strlen(SetAppKey));
                    }
                    else
                    {
                        return LORA_CONFIG_ERR;
                    }
                    __delay_cycles(100000);
                    bytesAvailable = numUartBytesReceived(&buffer);
                    if (bytesAvailable > 0)
                    {
                        status = readResponseFromLORA(bytesAvailable);
                        if (status == LORA_RESPONSE_SUCCESS)
                        {

                            status = LORA_NO_RESPONSE_BAD_PACKET;
                            buffer.txBufLen = 250;
                            uartSendDataInt(&buffer,
                                            (unsigned char *) SetSaveMac,
                                            strlen(SetSaveMac));
                            __delay_cycles(1000000);
                            bytesAvailable = numUartBytesReceived(&buffer);
                            if (bytesAvailable > 0)
                            {
                                status = readResponseFromLORA(bytesAvailable);
                                if (status == LORA_RESPONSE_SUCCESS)
                                {

                                    if (mode == ABP)
                                    {
                                        return joinMacABP();
                                    }
                                    else if (mode == OTAA)
                                    {
                                        return joinMacOTAA();
                                    }
                                    else
                                    {
                                        return LORA_CONFIG_ERR;
                                    }

                                }
                                else
                                {
                                    return LORA_NO_RESPONSE_BAD_PACKET;
                                }

                            }
                            else
                            {
                                return LORA_NO_RESPONSE_BAD_PACKET;
                            }

                        }
                        else
                        {
                            return LORA_NO_RESPONSE_BAD_PACKET;
                        }
                    }
                    else
                    {
                        return LORA_NO_RESPONSE_BAD_PACKET;
                    }

                }
                else
                {
                    return status;
                }
            }
            else
            {
                return LORA_NO_RESPONSE_BAD_PACKET;
            }
        }
        else
        {
            return status;
        }
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }

}

#if !LoraWAN
int setMacRadioPowerIndex(char* powerIndex)
{
    char transmitString[100];
    int bytesAvailable = -1;
    memset(transmitString, 0, 100);
    snprintf(transmitString, 100, SetMacRadioPwrID, powerIndex);
    token = strtok(transmitString, "$");
    initBufferDefaults(&buffer);

    setUartTxBuffer(&buffer, uartTxBuf, 250);
    setUartRxBuffer(&buffer, uartRxBuf, 250);

    uartSendDataBlocking(&buffer, (unsigned char *) token, strlen(token));
    __delay_cycles(100000);
    bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        return readResponseFromLORA(bytesAvailable);
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }

}
int loraWANMacPause()
{
    uartSendDataInt(&buffer, (unsigned char *) SetPauseMac,
                    strlen(SetPauseMac));
    __delay_cycles(100000);
    volatile int bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        int status = readResponseFromLORA(bytesAvailable);
        if (status == LORA_RESPONSE_SUCCESS)
        {
            return status;
        }
        else
        {
            return LORA_NO_RESPONSE_BAD_PACKET;
        }
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }
}

int loraWANMacResume()
{
    uartSendDataInt(&buffer, (unsigned char *) SetResumeMac,
                    strlen(SetResumeMac));
    __delay_cycles(100000);
    volatile int bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        int status = readResponseFromLORA(bytesAvailable);
        if (status == LORA_RESPONSE_SUCCESS)
        {
            return status;
        }
        else
        {
            return LORA_NO_RESPONSE_BAD_PACKET;
        }
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }
}

int radioTrasnmitOk()
{
    uartSendDataInt(&buffer, (unsigned char *) SetRadioTxOk,
                    strlen(SetRadioTxOk));
    __delay_cycles(100000);
    volatile int bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        return readResponseFromLORA(bytesAvailable);
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }
}
int radioTrasnmitUncfData(char* value)
{
    int status = loraWANMacPause();
    if (status == LORA_RESPONSE_SUCCESS)
    {
        status = LORA_NO_RESPONSE_BAD_PACKET;
        char transmitString[100];
        int bytesAvailable = -1;
        memset(transmitString, 0, 100);
        snprintf(transmitString, 100, SetRadioTxUncf, value);
        token = strtok(transmitString, "$");
        initBufferDefaults(&buffer);

        setUartTxBuffer(&buffer, uartTxBuf, 250);
        setUartRxBuffer(&buffer, uartRxBuf, 250);

        uartSendDataBlocking(&buffer, (unsigned char *) token, strlen(token));
        __delay_cycles(100000);
        bytesAvailable = numUartBytesReceived(&buffer);
        if (bytesAvailable > 0)
        {
            status = readResponseFromLORA(bytesAvailable);
            if (status == LORA_RESPONSE_SUCCESS)
            {
                status = radioTrasnmitOk();
                //status = loraWANMacResume();
                return status;
            }
            else
            {
                return LORA_NO_RESPONSE_BAD_PACKET;
            }
        }
        else
        {
            return LORA_NO_RESPONSE_BAD_PACKET;
        }

    }
    else
    {
        return status;
    }

}
#endif

int trasnmitUncfData(char* sequenceNumber, char* value)
{
    char transmitString[100];
    int status = LORA_NO_RESPONSE_BAD_PACKET;
    int bytesAvailable = -1;
    memset(transmitString, 0, 100);
    snprintf(transmitString, 100, SetTxUncf, sequenceNumber, value);
    token = strtok(transmitString, "$");
    initBufferDefaults(&buffer);

    setUartTxBuffer(&buffer, uartTxBuf, 250);
    setUartRxBuffer(&buffer, uartRxBuf, 250);

    uartSendDataBlocking(&buffer, (unsigned char *) token, strlen(token));
    __delay_cycles(100000);
    bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        status = readResponseFromLORA(bytesAvailable);
        if (status == LORA_RESPONSE_SUCCESS)
        {
            return status;
        }
        else
        {
            return LORA_NO_RESPONSE_BAD_PACKET;
        }
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }

}

int saveLoraGatewayConfigSave()
{
    uartSendDataInt(&buffer, (unsigned char *) SetSaveMac, strlen(SetSaveMac));
    __delay_cycles(100000);
    int bytesAvailable = numUartBytesReceived(&buffer);
    if (bytesAvailable > 0)
    {
        return readResponseFromLORA(bytesAvailable);
    }
    else
    {
        return LORA_NO_RESPONSE_BAD_PACKET;
    }

}
int readResponseFromLORA(int bytesAvailable)
{
    memset(tempBuf, 0, 100);
    volatile int bytesRead = readRxBytes(&buffer, tempBuf, bytesAvailable, 0);
    if (bytesRead == bytesAvailable)
    {
        if ((tempBuf[0] == invalidLoraResponse[0])
                && (tempBuf[7] == invalidLoraResponse[7])
                && (tempBuf[12] == invalidLoraResponse[12]))
        {
            return LORA_INVALID_PARAM;
        }
        if ((tempBuf[0] == 'e') && (tempBuf[1] == 'r') && (tempBuf[2] == 'r'))
        {
            return LORA_MAC_CONFIG_ERR;
        }
        return LORA_RESPONSE_SUCCESS;
    }
    else
    {
        return LORA_NO_RESPONSE;
    }
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=USCI_A0_VECTOR
__interrupt void USCI_A0_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(USCIAB0RX_VECTOR))) USCI0RX_ISR (void)
#else
#error Compiler not supported!
#endif
{
    switch (__even_in_range(UCA0IV, 4))
    {
    case 0:
        break;                             // Vector 0 - no interrupt
    case 2:                                // Vector 2 - RXIFG
        buffer.rxBuf[buffer.rxBytesReceived] = UCA0RXBUF;
        buffer.rxBytesReceived++;

        // If the received bytes filled up the buffer, go back to beginning
        if (buffer.rxBytesReceived > buffer.rxBufLen)
        {
            buffer.rxBytesReceived = 0;
        }
        break;
    case 4:                               // Vector 4 - TXIFG
        if (buffer.txBytesToSend > 0)
        {
            UCA0TXBUF = buffer.txBuf[buffer.txBufCtr];
            buffer.txBufCtr++;

            // If we've sent all the bytes, set counter to 0 to stop the sending
            if (buffer.txBufCtr == buffer.txBytesToSend)
            {
                buffer.txBufCtr = 0;

                // Disable TX IE
                UCA0IE &= ~UCTXIE;

                // Clear TX IFG
                UCA0IFG &= ~UCTXIFG;

            }
        }
        break;
    default:
        break;
    }
}
