################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../lnk_msp430f5529.cmd 

C_SRCS += \
../initPortsAsOutputs.c \
../loraSerialCommunication.c \
../main.c \
../sensorActivatingPulse.c \
../sensorAdc.c \
../uart.c 

C_DEPS += \
./initPortsAsOutputs.d \
./loraSerialCommunication.d \
./main.d \
./sensorActivatingPulse.d \
./sensorAdc.d \
./uart.d 

OBJS += \
./initPortsAsOutputs.obj \
./loraSerialCommunication.obj \
./main.obj \
./sensorActivatingPulse.obj \
./sensorAdc.obj \
./uart.obj 

OBJS__QUOTED += \
"initPortsAsOutputs.obj" \
"loraSerialCommunication.obj" \
"main.obj" \
"sensorActivatingPulse.obj" \
"sensorAdc.obj" \
"uart.obj" 

C_DEPS__QUOTED += \
"initPortsAsOutputs.d" \
"loraSerialCommunication.d" \
"main.d" \
"sensorActivatingPulse.d" \
"sensorAdc.d" \
"uart.d" 

C_SRCS__QUOTED += \
"../initPortsAsOutputs.c" \
"../loraSerialCommunication.c" \
"../main.c" \
"../sensorActivatingPulse.c" \
"../sensorAdc.c" \
"../uart.c" 


