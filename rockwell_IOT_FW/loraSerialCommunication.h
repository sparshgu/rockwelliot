/*
 * loraSerialCommunication.h
 *
 *  Created on: Nov 9, 2018
 *      Author: User
 */

#ifndef LORASERIALCOMMUNICATION_H_
#define LORASERIALCOMMUNICATION_H_

enum LORA_ERR_CODES
{
    LORA_RESPONSE_SUCCESS = 0,
    LORA_NO_RESPONSE = 1,
    LORA_NO_RESPONSE_BAD_PACKET = 2,
    LORA_INVALID_PARAM = 3,
    LORA_MAC_CONFIG_ERR = 4,
    LORA_CONFIG_ERR = 5
};
enum GATEWAY_CONNECTION_MODES
{
    ABP = 0, OTAA = 1
};

#if !LoraWAN
#define RadioPower_2  "2"
#define RadioPower_3  "3"
#define RadioPower_4  "4"
#define RadioPower_5  "5"
#define RadioPower_6  "6"
#define RadioPower_7  "7"
#define RadioPower_8  "8"
#define RadioPower_9  "9"
#define RadioPower_10  "10"
#define RadioPower_11  "11"
#define RadioPower_12  "12"
#define RadioPower_13  "13"
#define RadioPower_14  "14"
#define RadioPower_15  "15"
#define RadioPower_16  "16"
#define RadioPower_17  "17"
#define RadioPower_18  "18"
#define RadioPower_19  "19"
#define RadioPower_20  "20"
#define BW_125  "125"
#define BW_250  "250"
#define BW_500  "500"
#define SF7 "sf7"
#define SF8 "sf8"
#define SF9 "sf9"
#define SF10 "sf10"
#define SF11 "sf11"
#define SF12 "sf12"
static const char SetPauseMac[] = "mac pause\r\n";
static const char SetRadioPwr[] = "radio set pwr %s\r\n$";
static const char SetRadioCRC[] = "radio set crc on\r\n";
static const char SetRadioCR[] = "radio set cr 4/5\r\n";
static const char SetRadioMode[] = "radio set mod lora\r\n";
static const char SetRadioBW[] = "radio set bw %s\r\n$";
static const char SetRadioCenterFreq[] = "radio set freq %s\r\n$";
static const char SetRadioSpread[] = "radio set sf %s\r\n$";
static const char SetRadioTxUncf[] = "radio tx %s\r\n$";
static const char SetRadioTxOk[] = "radio_tx_ok\r\n$";
static const char SetResumeMac[] = "mac resume\r\n";
int setRadioPower(char*);
int setRadioBandWidth(char*);
int setRadioCenterFreq(char*);
int setRadioSpread(char*);
#endif
static unsigned char invalidLoraResponse[] = "invalid_param";
static const char SetNetworkKey[] =
        "mac set nwkskey 2b7e151628aed2a6abf7158809cf4f3c\r\n";
static const char SetAppsKey[] =
        "mac set appskey 3C8F262739BFE3B7BC0826991AD0504D\r\n";
static const char SetMacRadioPwrID[] = " mac set pwridx %s\r\n$";
static const char SetDeviceAddress[] = "mac set devaddr 001C0EE9\r\n";
static const char SetDevEui[] = "mac set deveui 0000000000000109\r\n";
static const char SetAppEui[] = "mac set appeui 00000000000098FE\r\n";
static const char SetAppKey[] =
        "mac set appkey 0000000000000000000000000000010F\r\n";
static const char SetmacOTAAJoin[] = "mac join otaa\r\n";
static const char SetSaveMac[] = "mac save\r\n";
static const char SetJoinABP[] = "mac join abp\r\n";
static const char SetTxUncf[] = "mac tx uncnf %s %s\r\n$";
static const char SetMacReset[] = "mac reset\r\n";
static const char GetVer[] = "sys get ver\r\n";
static const char GetVdd[] = "sys get vdd\r\n";
static const char PressureSensorSequence[] = "76"; /*Pressure Sensor values sequence number*/
void setUpLoraUARTCommunication();
int resetLoraRNModule();
int loraWANMacPause();
int loraWANMacResume();
int radioTrasnmitUncfData(char*);
int setUpLoraWANToCommunicateWithGateway(int);
int setUpLoraToCommunicateWithGateway();
int saveLoraGatewayConfigSave();
int readResponseFromLORA(int bytesAvailable);
int joinMacOTAA();
int joinMacABP();
int getSystemVdd();
int getSystemVer();
int trasnmitUncfData(char* sequenceNumber, char* value);

#endif /* LORASERIALCOMMUNICATION_H_ */
