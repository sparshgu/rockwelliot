/* --COPYRIGHT--,BSD
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/
#include <stdlib.h>
#include "driverlib.h"
#include "board.h"
#include "sensorAdc.h"
#include "initPorts.h"
#include "sensorActivatingPulse.h"
#include "loraSerialCommunication.h"
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

#define N_DECIMAL_POINTS_PRECISION (1000)

char *atohx(char * dst, const char * src)
{
    char * ret = dst;
    int i;
    for (i = 0; i < strlen(src); ++i)
    {

        int high = src[i] >> 4;
        int low = src[i] & 0xF;
        *ret = high + '0' + (7 * (high / 10));
        *(ret + 1) = low + '0' + (7 * (low / 10));
        ret += 2;

    }
    *ret = '\0';
    return ret;
}

void getAsciiCharArrayForPressure(float value, char* integralPart,
                                  char* fractionalPart)
{
    value = value + 0.01 * value;
    int integerPart = (int) value;
    int decimalPart = ((int) (value * N_DECIMAL_POINTS_PRECISION)
            % N_DECIMAL_POINTS_PRECISION);
    sprintf(integralPart, "%d", integerPart);
    sprintf(fractionalPart, "%d", decimalPart);
}

void main(void)
{
    char pressureSensorValues[35];
    char pressureSensorValuesHex[70];
    memset(pressureSensorValues, 0, 70);
#if REAL_VALUES
    char intPart[3][8];
    memset(intPart, 0, 96);

    char fracPart[3][8];
    memset(fracPart, 0, 96);
#endif

    //Stop WDT
    WDT_A_hold(WDT_A_BASE);

    initPorts();

    setPulseOutputPin();

    configureTimerForSensorPulse();

    configureADC();

    setUpLoraUARTCommunication();

    //resetLoraRNModule();
    __enable_interrupt();
    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0);
    GPIO_setOutputLowOnPin(RED_LED);
    volatile int status = -1;

#if LoraWAN
#if IsABP
    status = setUpLoraWANToCommunicateWithGateway(ABP);
#else
    status = setUpLoraWANToCommunicateWithGateway(OTAA);
#endif
#else
    status = setUpLoraToCommunicateWithGateway();
#endif
    if (status != LORA_RESPONSE_SUCCESS)
    {
        GPIO_setOutputHighOnPin(RED_LED);
        return;
    }

    outputPulse();

    while (1)
    {
        if (adcSensorEnable == 0)
        {
            startADC();
        }
        if (adcSensorEnable == 0) // && adcSensor2Enable == 0
        //&& adcSensor3Enable == 0)
        {

#if REAL_VALUES
            if ((results[0] + 70) >= 736)
            {
                getAsciiCharArrayForPressure(
                        ((0.169 * (results[0] + 70)) - 124.276), intPart[0],
                        fracPart[0]);
            }
            else
            {
                strncpy(intPart[0], sensorNCorInvalidval,
                        strlen(sensorNCorInvalidval) + 1);
                strncpy(fracPart[0], sensorNCorInvalidval,
                        strlen(sensorNCorInvalidval) + 1);
            }
            if ((results[1] + 100) >= 736)
            {
                getAsciiCharArrayForPressure(
                        ((0.169 * (results[1] + 100)) - 124.276), intPart[1],
                        fracPart[1]);
            }
            else
            {
                strncpy(intPart[1], sensorNCorInvalidval,
                        strlen(sensorNCorInvalidval) + 1);
                strncpy(fracPart[1], sensorNCorInvalidval,
                        strlen(sensorNCorInvalidval) + 1);
            }
            if ((results[2] + 120) >= 736)
            {
                getAsciiCharArrayForPressure(
                        ((0.169 * (results[2] + 130)) - 124.276), intPart[2],
                        fracPart[2]);
            }
            else
            {
                strncpy(intPart[2], sensorNCorInvalidval,
                        strlen(sensorNCorInvalidval) + 1);
                strncpy(fracPart[2], sensorNCorInvalidval,
                        strlen(sensorNCorInvalidval) + 1);
            }
            memset(pressureSensorValues, 0, 35);
            memset(pressureSensorValuesHex, 0, 70);
            snprintf(pressureSensorValues, 70, pressureSensorValuesTemplate,
                    "1", intPart[0], fracPart[0], "2", intPart[1], fracPart[1],
                    "3", intPart[2], fracPart[2]);
            atohx(pressureSensorValuesHex, pressureSensorValues);

#if (LoraWAN == 1)
            trasnmitUncfData((char*) PressureSensorSequence,
                    pressureSensorValuesHex);
#else
            radioTrasnmitUncfData(pressureSensorValuesHex);
#endif

#else
            memset(pressureSensorValues, 0, 35);
            memset(pressureSensorValuesHex, 0, 70);
            snprintf(pressureSensorValues, 70, "s%s:%d-s%s:%d-s%s:%d", "1",
                     results[0], "2", results[1], "3", results[2]);
            atohx(pressureSensorValuesHex, pressureSensorValues);
#if LoraWAN
            trasnmitUncfData((char*) PressureSensorSequence,
                             pressureSensorValuesHex);
#else
            radioTrasnmitUncfData(pressureSensorValuesHex);
#endif

#endif

        }
        //Enter LPM4, Enable interrupts
        __bis_SR_register(LPM3_bits + GIE);
        //For debugger
        //__no_operation();
    }

}
