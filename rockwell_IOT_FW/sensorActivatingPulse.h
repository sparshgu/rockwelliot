/*
 * sensorActivatingPulse.h
 *
 *  Created on: Nov 9, 2018
 *      Author: User
 */

#ifndef SENSORACTIVATINGPULSE_H_
#define SENSORACTIVATINGPULSE_H_

#include "driverlib.h"

#define TIMER_PERIOD 13105
#define DUTY_CYCLE   90

extern volatile int adcSensorEnable;

typedef struct
{
    Timer_A_outputPWMParam* param0;
    Timer_A_outputPWMParam* param1;
    Timer_A_outputPWMParam* param2;
}OutputPWMParams;



void setPulseOutputPin();
void configureTimerForPWM(OutputPWMParams* param);
void outputPWM(Timer_A_outputPWMParam* param);
void configureTimerForSensorPulse();
void outputPulse();
#endif /* SENSORACTIVATINGPULSE_H_ */
