/*
 * board.h
 *
 *  Created on: Nov 10, 2018
 *      Author: User
 */

#ifndef BOARD_H_
#define BOARD_H_

#define RED_LED         GPIO_PORT_P1,GPIO_PIN0
#define GREEN_LED       GPIO_PORT_P4,GPIO_PIN7
#define REAL_VALUES     0
#define IsABP           1
#define LoraWAN         0
static const char sensorNCorInvalidval[] = "nc";
static const char pressureSensorValuesTemplate[] = "s%s:%s.%s-s%s:%s.%s-s%s:%s.%s";


#endif /* BOARD_H_ */
